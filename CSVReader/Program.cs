﻿using System;
using System.Globalization;
using CsvHelper;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;

namespace CSVReader
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var streamReader = new StreamReader(@"C:\Users\braam\Downloads\username.csv"))
            {
                using (var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture))
                {
                    //csvReader.Context.RegisterClassMap<UsersClassMap>();
                    
                    var records = csvReader.GetRecords<Users>().ToList();
                    var user= records.Find(x => x.Firstname == "johnson81");
                }
            }
            Console.WriteLine("Hello World!");
        }
    }

    //public class UsersClassMap : ClassMap<Users>
    //{
    //    public UsersClassMap()
    //    {
    //        Map(m => m.Username).Name("Username");
    //        Map(m => m.Identifier).Name("Identifier");
    //        Map(m => m.Firstname).Name("First_name");
    //        Map(m => m.Lastname).Name("Last_name");
    //    }
    //}

    public class Users
    {
        [Name("Username")]
        public string Username { get; set; }
        [Name("Identifier")]
        public int Identifier { get; set; }
        [Name("First_name")]
        public string Firstname { get; set; }
        [Name("Last_name")]
        public string Lastname { get; set; }
    }
}
